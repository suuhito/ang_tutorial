import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const appRoutes: Routes = [
    {
        path: 'application',
        loadChildren: 'app/application/application.module#ApplicationModule',
    },
    {
        path: '',
        redirectTo: 'application',
        pathMatch: 'full'
    },
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes, { useHash: true }),
    ],
    exports: [
        RouterModule
    ],
    providers: []
})
export class AppRoutingModule { }
