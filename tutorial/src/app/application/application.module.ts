import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';

import { ApplicationRoutingModule } from './application.routing';

import { ApplicationComponent } from './application/application.component';
import { TopComponent } from './application/top/top.component';
import { GeneralComponent } from './application/general/general.component';
import { HoujinComponent } from './application/houjin/houjin.component';



@NgModule({
    imports: [ ApplicationRoutingModule, ReactiveFormsModule, CommonModule , HttpModule],
    exports: [],
    declarations: [
        ApplicationComponent,
        TopComponent,
        GeneralComponent,
        HoujinComponent,
        ],
    providers: [],
})
export class ApplicationModule { }
