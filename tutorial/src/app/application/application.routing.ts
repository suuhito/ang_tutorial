import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ApplicationComponent } from './application/application.component';
import { TopComponent } from './application/top/top.component';
import { GeneralComponent } from './application/general/general.component';
import { HoujinComponent } from './application/houjin/houjin.component';

const routes: Routes = [
    {
        path: '',
        component: ApplicationComponent,
        children: [
            {
                path: '',
                component: TopComponent,
            },
            {
              path: 'general',
              component: GeneralComponent,
            },
            {
                path: 'houjin',
                component: HoujinComponent,
            }
        ]
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ],
    providers: []
})
export class ApplicationRoutingModule { }
